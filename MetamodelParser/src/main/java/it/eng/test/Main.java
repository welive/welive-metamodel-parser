package it.eng.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.swagger.util.Json;
import it.eng.metamodel.parser.MetamodelParser;
import it.eng.metamodel.parser.ParserResponse;
import it.eng.metamodel.utils.Utils;

public class Main {

	
	
	public static void main(String[] args) throws JSONException 
	{
		FileInputStream fisTargetFile;
		String descriptor = "";
		
		try 
		{
			fisTargetFile = new FileInputStream(new File("/users/chiara/desktop/provaVuoto.json"));
			
			
			descriptor = IOUtils.toString(fisTargetFile, "UTF-8");
			
			
			
			
			//System.out.println(descriptor);
		} 
		catch (FileNotFoundException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		/*
		ParserResponse response = MetamodelParser.parse(descriptor, MetamodelParser.SourceType.WADL);
		
		try 
		{
			System.out.println("ERRORS: " + response.getErrors().toString());
			System.out.println("WARNINGS: " + response.getWarnings().toString());
			Json.prettyPrint(response.getBuildingBlock());
			
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		
		
		System.out.println("OUTPUT:\n\n" + MetamodelParser.convertDescriptor(descriptor, MetamodelParser.SourceType.METAMODEL, MetamodelParser.SourceType.SWAGGER));
		//System.out.println("INPUT: \n\n" + descriptor);
		//System.out.println(MetamodelParser.parse(descriptor, MetamodelParser.SourceType.WADL).getBuildingBlock().getOperations());
		//Json.prettyPrint(MetamodelParser.parse(descriptor, MetamodelParser.SourceType.SWAGGER));
		
		//List<String> prova = splitDescriptor(descriptor);
		
		
	}
	
	
}
