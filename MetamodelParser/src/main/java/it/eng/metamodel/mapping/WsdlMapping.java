package it.eng.metamodel.mapping;

import it.eng.metamodel.definitions.BuildingBlock;
import it.eng.metamodel.definitions.Entity;
import it.eng.metamodel.definitions.License;
import it.eng.metamodel.definitions.ServiceOffering;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.predic8.wsdl.Binding;
import com.predic8.wsdl.BindingOperation;
import com.predic8.wsdl.Definitions;
import com.predic8.wsdl.Fault;
import com.predic8.wsdl.Message;
import com.predic8.wsdl.Operation;
import com.predic8.wsdl.Part;
import com.predic8.wsdl.Port;
import com.predic8.wsdl.Service;
import com.predic8.wsdl.WSDLParser;

public class WsdlMapping implements MetamodelMapping 
{
	private static final Logger logger = LoggerFactory.getLogger(WsdlMapping.class);
	
	
	public MappingResponse validateDescriptor(String descriptor)
	{
		return _validateDescriptor(descriptor);
	}

	
	public MappingResponse validateDescriptor(Object descriptor)
	{
		String d;
		try 
		{
			d = new ObjectMapper().writeValueAsString((Definitions) descriptor);
		} 
		catch (JsonProcessingException e) 
		{
			logger.error(e.getMessage());
			
			MappingResponse response = new MappingResponse();
			response.addError(e.getMessage());
			
			return response;
		}
		
		return _validateDescriptor(d);
	}

	
	public MappingResponse preprocessDescriptor(String descriptor) 
	{
		WSDLParser parser = new WSDLParser();
		
		Definitions wsdl = null;
		
		try 
		{
			wsdl = parser.parse(IOUtils.toInputStream(descriptor, "UTF-8"));
		} 
		catch (Exception e) 
		{
			logger.error(e.getMessage());
			
			MappingResponse mappingResponse = new MappingResponse();
			mappingResponse.addError(e.getMessage());
			return mappingResponse;
		}
		
		return preprocessDescriptor(wsdl);
	}

	
	public MappingResponse preprocessDescriptor(Object descriptor)
	{
		MappingResponse response = new MappingResponse();
		
		response.setDescriptor(descriptor);
		response.setDescriptorClass(getDescriptorClass());
		
		return response;
	}

	
	public MappingResponse toBuildingBlock(String descriptor)
	{
		WSDLParser parser = new WSDLParser();
		
		Definitions wsdl = null;
		
		try 
		{
			wsdl = parser.parse(IOUtils.toInputStream(descriptor, "UTF-8"));
		} 
		catch (Exception e) 
		{
			logger.error(e.getMessage());
			
			MappingResponse mappingResponse = new MappingResponse();
			mappingResponse.addError(e.getMessage());
			return mappingResponse;
		}
		
		return _toBuildingBlock(wsdl);
	}

	
	public MappingResponse toBuildingBlock(Object descriptor) 
	{
		return _toBuildingBlock(descriptor);
	}

	
	public Class<?> getDescriptorClass() 
	{
		return Definitions.class;
	}
	
	private MappingResponse _validateDescriptor(String descriptor)
	{
		MappingResponse response = new MappingResponse();
		
		InputStream main_xsd = null;
		
		String xsdFile = "wsdl-schema.xsd";
		
		try 
		{
			main_xsd = WadlMapping.class.getClassLoader().getResourceAsStream(xsdFile);
		} 
		catch (Exception e) 
		{
			logger.error("Error reading " + xsdFile + ":" + e.getMessage());	

			response.addError(e.getMessage());			
			return response;
		}
		
	    try
	    {	    	
	        SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
	        
	        Schema schema = factory.newSchema(new StreamSource(main_xsd));
	        
	        Validator validator = schema.newValidator();
	        validator.setErrorHandler(new XsdErrorHandler());
	        validator.validate(new StreamSource(new ByteArrayInputStream(descriptor.getBytes())));	        
	    }
	    catch(SAXException ex)
	    {
	    	logger.error("Validation error: " + ex.getMessage());
	    	response.addError(ex.getMessage());
	    	return response;
	    } 
	    catch (IOException e) 
	    {
	    	logger.error("Error validating xml");
	    	response.addError(e.getMessage());
	    	return response;
		} 
	    
	    return response;
	}
	
	private MappingResponse _toBuildingBlock(Object descriptor)
	{
		Definitions wsdl = (Definitions) descriptor;
		
		MappingResponse mappingResponse = new MappingResponse();
		
		BuildingBlock artefact = new BuildingBlock();
		
		try
		{		
			// General info		
			artefact.setType("SOAP");		
			
			if(wsdl.getServices().size() == 0)
			{
				mappingResponse.addError("no service defined"); 
				return mappingResponse;
			}
			
			if(wsdl.getName() != null && wsdl.getName().compareTo("") != 0)
				artefact.setTitle(wsdl.getName());
			else
				artefact.setTitle(wsdl.getServices().size() == 1 ? wsdl.getServices().get(0).getName() : "");
			
			if(wsdl.getDocumentation() != null)
				artefact.setDescription(wsdl.getDocumentation().getContent());
			else
				artefact.setDescription(wsdl.getServices().get(0).getDocumentation() != null ? wsdl.getServices().get(0).getDocumentation().getContent() : "");
			
			artefact.setAbstractDescription(wsdl.getDocumentation() != null ? wsdl.getDocumentation().getContent() : "");
			artefact.setPilot("");
			artefact.setCreated("");
			artefact.setPage("");
			
			List<String> tags = new ArrayList<String>();		
			artefact.setTags(tags);
			//
			
			// Business Roles
			artefact.setBusinessRoles(new ArrayList<Entity>());
			// Service Offering
			artefact.setServiceOfferings(new ArrayList<ServiceOffering>());
			
			// Legal Condition
			List<License> licenses = new ArrayList<License>();	
			artefact.setLegalConditions(licenses);
			//	
			
			// descriptor
			artefact.setDescriptorType("WSDL");
			
			// Interaction Points
			//List<InteractionPoint> intPoints = new ArrayList<InteractionPoint>();
			//InteractionPoint intPoint = null;
			
			// variables to reuse into loops		
			// Artefact variables
			List<it.eng.metamodel.definitions.Operation> bbOperations = null;
			it.eng.metamodel.definitions.Operation bbOperation = null;
			
			List<it.eng.metamodel.definitions.Parameter> parameters = null;
			it.eng.metamodel.definitions.Parameter parameter = null;
			List<it.eng.metamodel.definitions.Response> responses = null;
			it.eng.metamodel.definitions.Response response = null;	
			List <String> produces;
			// wsdl variables		
			Service service = null;
			Iterator<Port> ports = null;
			Port port = null;
			Binding binding = null;
			Iterator<BindingOperation> bindingOperations = null;
			BindingOperation bindingOperation = null;
			Operation operation = null;
			Message message = null;
			Iterator<Part> parts = null;
			Part part = null;
			Iterator<Fault> faults = null;
			Fault fault = null;
			//
			
			bbOperations = new ArrayList<it.eng.metamodel.definitions.Operation>();
			
			String baseUrl = "";
			
			// iterate through services
			
			Iterator<Service> services = wsdl.getServices().iterator();
			
			while(services.hasNext())
			{
				service = services.next();
				
				if(service.getPorts().size() == 0)
				{
					mappingResponse.addError("service '" + service.getName() + "' doesn't contain ports"); 
					return mappingResponse;
				}
				
				// iterate through service ports
				
				ports = service.getPorts().iterator();
				
				while(ports.hasNext())
				{
					port = ports.next();
					
					// define new interaction point (service-port-binding)
					//intPoint = new InteractionPoint();
					//bbOperations = new ArrayList<it.eng.metamodel.definitions.Operation>();
					
					if(port.getAddress() == null || port.getAddress().getLocation().compareTo("") == 0)
					{
						mappingResponse.addError("port '" + port.getName() + "' has no address location"); 
						return mappingResponse;
					}
					
					//intPoint.setBaseurl(port.getAddress().getLocation());		
					baseUrl = port.getAddress().getLocation();
					
					binding = port.getBinding();
					
					if(binding == null)
					{
						mappingResponse.addError("port '" + port.getName() + "' has no binding"); 
						return mappingResponse;
					}
					
					if(binding.getOperations().size() == 0)
					{
						mappingResponse.addWarning("port '" + binding.getName() + "' doesn't contain operations"); 
					}
					
					bindingOperations = binding.getOperations().iterator();
					
					// iterate through binding operations
					
					while(bindingOperations.hasNext())
					{
						bindingOperation = bindingOperations.next();
						// retrieve operation from binding operation
						operation = wsdl.getOperation(bindingOperation.getName(), binding.getType());
						
						if(operation == null)
						{
							mappingResponse.addError("no operation defined for binding operation '" + bindingOperation.getName() + "'"); 
							return mappingResponse;
						}
						
						bbOperation = new it.eng.metamodel.definitions.Operation();
						
						bbOperation.setTitle(operation.getName());
						bbOperation.setDescription(bindingOperation.getDocumentation() != null ? bindingOperation.getDocumentation().getContent() : "");
						//bbOperation.setPath(bindingOperation.getOperation().getSoapAction());
						bbOperation.setEndpoint(baseUrl + (bindingOperation.getOperation().getSoapAction().charAt(0) == '/' ? "" : "/") + bindingOperation.getOperation().getSoapAction());
						produces = new ArrayList<String>();
						produces.add("application/soap+xml");
						bbOperation.setConsumes("application/soap+xml");
						bbOperation.setProduces(produces);
						bbOperation.setMethod("POST");
						
						parameters = new ArrayList<it.eng.metamodel.definitions.Parameter>();
						
						responses = new ArrayList<it.eng.metamodel.definitions.Response>();
						response = new it.eng.metamodel.definitions.Response();
						
						if(operation.getInput() == null && operation.getOutput() == null)
						{
							mappingResponse.addWarning("operation '" + operation.getName() + "' hasn't input nor output"); 
						}
						
						if(operation.getInput() != null)
						{
							message = operation.getInput().getMessage();
							parts = message.getParts().iterator();
							
							// iterate through input message parts
							
							while(parts.hasNext())
							{
								part = parts.next();
								
								parameter = new it.eng.metamodel.definitions.Parameter();
								parameter.setName(message.getName() + (part.getName() != null && part.getName() != "" ? ":" + part.getName() : ""));
								
								if(part.getElement() != null)
								{
									parameter.setSchema(part.getElement().getAsString().replaceAll("\n *", ""));
									parameter.setType("element:" + part.getElement().getName());
								}
								else
								{
									parameter.setType(part.getType().getQname().getLocalPart());
								}
								
								parameter.setIn("body");
								parameter.setRequired(true);
								parameter.setDescription(message.getDocumentation() != null ? message.getDocumentation().getContent() : "");
								
								parameters.add(parameter);
							}					
						}
						
						if(operation.getOutput() != null)
						{
							message = operation.getOutput().getMessage();
							parts = message.getParts().iterator();
							
							// iterate through output message parts
							
							while(parts.hasNext())
							{
								part = parts.next();
								
								response = new it.eng.metamodel.definitions.Response();
						
								if(part.getElement() != null)
								{
									response.setSchema(part.getElement().getAsString().replaceAll("\n *", ""));
									response.setType("element:" + part.getElement().getName());
								}
								else
								{
									response.setType(part.getType().getQname().getLocalPart());
								}
								
								response.setCode(200);
								response.setDescription(message.getDocumentation() != null ? message.getDocumentation().getContent() : "");
								
								responses.add(response);
							}							
						}
						
						if(operation.getFaults() != null && operation.getFaults().size() > 0)
						{
							faults = operation.getFaults().iterator();
							
							// iterate through fault messages
							
							while(faults.hasNext())
							{
								fault = faults.next();
								
								message = fault.getMessage();
								parts = message.getParts().iterator();
								
								while(parts.hasNext())
								{
									part = parts.next();
									
									response = new it.eng.metamodel.definitions.Response();
							
									if(part.getElement() != null)
									{
										response.setSchema(part.getElement().getAsString().replaceAll("\n *", ""));
										response.setType("element:" + part.getElement().getName());
									}
									else
									{
										response.setType(part.getType().getQname().getLocalPart());
									}
									
									response.setDescription(message.getDocumentation() != null ? message.getDocumentation().getContent() : "");
									
									responses.add(response);
								}
							}
						}
						
						bbOperation.setParameters(parameters);
						bbOperation.setResponses(responses);
						
						bbOperations.add(bbOperation);
					}
					
					//intPoint.setOperations(bbOperations);
					//intPoints.add(intPoint);
				}				
			}
			
			//artefact.setInteractionPoint(intPoints);		
			artefact.setOperations(bbOperations);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			mappingResponse.addError(e.getMessage()); 
			return mappingResponse;
		}
		
		mappingResponse.setBuildingBlock(artefact);
		mappingResponse.setDescriptor(wsdl);
		mappingResponse.setDescriptorClass(wsdl.getClass());
		
		return mappingResponse;
	}
	
	class XsdErrorHandler implements ErrorHandler {

	    
	    public void warning(SAXParseException exception) throws SAXException {
	        handleMessage("Warning", exception);
	    }

	    
	    public void error(SAXParseException exception) throws SAXException {
	        handleMessage("Error", exception);
	    }

	    
	    public void fatalError(SAXParseException exception) throws SAXException {
	        handleMessage("Fatal", exception);
	    }

	    private String handleMessage(String level, SAXParseException exception) throws SAXException {
	        int lineNumber = exception.getLineNumber();
	        int columnNumber = exception.getColumnNumber();
	        String message = exception.getMessage();
	        throw new SAXException("[" + level + "] line: " + lineNumber + "; column: " + columnNumber + "; cause: " + message);
	    }
	}
}
