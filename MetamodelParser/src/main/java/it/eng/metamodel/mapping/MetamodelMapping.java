package it.eng.metamodel.mapping;

public interface MetamodelMapping 
{	
	public MappingResponse validateDescriptor(String descriptor);
	public MappingResponse validateDescriptor(Object descriptor);
	
	public MappingResponse preprocessDescriptor(String descriptor);
	public MappingResponse preprocessDescriptor(Object descriptor);
	
	public MappingResponse toBuildingBlock(String descriptor);		
	public MappingResponse toBuildingBlock(Object descriptor);
	
	public Class<?> getDescriptorClass();
}
