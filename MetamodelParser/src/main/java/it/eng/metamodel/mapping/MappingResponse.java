package it.eng.metamodel.mapping;

import java.util.ArrayList;
import java.util.List;

import it.eng.metamodel.definitions.BuildingBlock;

public class MappingResponse 
{
	private BuildingBlock buildingBlock;
	private Object descriptor;
	private Class<?> descriptorClass;
	private List<String> errors;
	private List<String> warnings;
	
	public MappingResponse() {
		super();
		this.buildingBlock = null;
		this.descriptor = null;
		this.errors = new ArrayList<String>();
		this.warnings = new ArrayList<String>();
	}

	public BuildingBlock getBuildingBlock() {
		return buildingBlock;
	}

	public void setBuildingBlock(BuildingBlock buildingBlock) {
		this.buildingBlock = buildingBlock;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public List<String> getWarnings() {
		return warnings;
	}

	public void setWarnings(List<String> warnings) {
		this.warnings = warnings;
	}
	
	public void addError(String err) {
		errors.add(err);
	}
	
	public void addWarning(String warn) {
		warnings.add(warn);
	}

	public Object getDescriptor() {
		return descriptor;
	}

	public void setDescriptor(Object descriptor) {
		this.descriptor = descriptor;
	}

	public Class<?> getDescriptorClass() {
		return descriptorClass;
	}

	public void setDescriptorClass(Class<?> descriptorClass) {
		this.descriptorClass = descriptorClass;
	}
}
