package it.eng.metamodel.mapping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.swagger.models.ExternalDocs;
import io.swagger.models.Info;
import io.swagger.models.Path;
import io.swagger.models.Scheme;
import io.swagger.models.Swagger;
import io.swagger.models.Tag;
import io.swagger.models.parameters.BodyParameter;
import io.swagger.models.parameters.FormParameter;
import io.swagger.models.parameters.HeaderParameter;
import io.swagger.models.parameters.PathParameter;
import io.swagger.models.parameters.QueryParameter;
import io.swagger.util.Json;
import it.eng.metamodel.definitions.AuthenticationMeasure;
import it.eng.metamodel.definitions.BuildingBlock;
import it.eng.metamodel.definitions.License;
import it.eng.metamodel.definitions.Operation;
import it.eng.metamodel.definitions.Parameter;
import it.eng.metamodel.definitions.Permission;
import it.eng.metamodel.definitions.Response;
import it.eng.metamodel.parser.MetamodelParser.SourceType;
import it.eng.metamodel.utils.Utils;


public class BuildingBlockMapping implements MetamodelMapping
{
	
	public MappingResponse validateDescriptor(String descriptor) 
	{
		return new MappingResponse();
	}
	
	
	public MappingResponse validateDescriptor(Object descriptor) 
	{
		return new MappingResponse(); 
	}

	
	public MappingResponse preprocessDescriptor(String descriptor) 
	{
		MappingResponse mappingResponse = new MappingResponse();
		BuildingBlock buildingBlock = null;
		
		try
		{
			
			
			buildingBlock = new ObjectMapper().readValue(descriptor, BuildingBlock.class);
		
		}
		catch(Exception e)
		{
			
			mappingResponse.addError(e.getMessage());
			
			return mappingResponse;
		}
		
		
		return preprocessDescriptor(buildingBlock);
	}
	
	
	public MappingResponse preprocessDescriptor(Object descriptor) 
	{
		MappingResponse response = new MappingResponse();
		
		response.setDescriptor(descriptor);
		response.setDescriptorClass(getDescriptorClass());
		
		return response;
	}
	
	
	
	
	
	public MappingResponse toBuildingBlock(String descriptor) 
	{
		MappingResponse mappingResponse = new MappingResponse();
		BuildingBlock buildingBlock = null;
		
		try
		{
			
			buildingBlock = new ObjectMapper().readValue(descriptor, BuildingBlock.class);
		}
		catch(Exception e)
		{
			mappingResponse.addError(e.getMessage());
			return mappingResponse;
		}
		
		return toBuildingBlock(buildingBlock);
	}

	
	public MappingResponse toBuildingBlock(Object descriptor) 
	{
		MappingResponse mappingResponse = new MappingResponse();
		
		mappingResponse.setDescriptor(descriptor);
		mappingResponse.setDescriptorClass(getDescriptorClass());
		mappingResponse.setBuildingBlock((BuildingBlock) descriptor);
		
		return mappingResponse;
	}

	
	public Class<?> getDescriptorClass() 
	{
		return BuildingBlock.class;
	}
	
	/* Reverse mapping **/
	
	public String describeAs(BuildingBlock bb, SourceType outType)
	{
		String res = "";
		List <String> results = new ArrayList<String>();
		
		
		
		try
		{
			if(outType == SourceType.METAMODEL)
			{
				res = new ObjectMapper().writeValueAsString(bb);
			}
			else if(outType == SourceType.SWAGGER)
			{
				
				res = describeAsSwagger(bb);
				System.out.println("stampo un risultato "+ res);
			}
			else if(outType == SourceType.WADL)
			{
				res = describeAsWadl(bb);
			}
			else if(outType == SourceType.WSDL)
			{
				res = describeAsWsdl(bb);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			
			return null;
		}
		
		return res;
	}
	
	private io.swagger.models.properties.AbstractProperty _buildProperty(ObjectNode node) throws Exception
	{
		io.swagger.models.properties.AbstractProperty property = null;
		
		String type = node.get("type").asText();
		
		if(type.compareTo("string") == 0)
		{
			property = new io.swagger.models.properties.StringProperty();			
			
			if(node.has("pattern"))
				((io.swagger.models.properties.StringProperty) property).setPattern(node.get("pattern").asText());
			
			if(node.has("maxLength"))
				((io.swagger.models.properties.StringProperty) property).setMaxLength(node.get("maxLength").asInt());
			
			if(node.has("minLength"))
				((io.swagger.models.properties.StringProperty) property).setMinLength(node.get("minLength").asInt());
			
			if(node.has("enum"))
			{
				ArrayNode enums = (ArrayNode) node.get("enum");
				
				List<String> _enum = new ArrayList<String>();
				
				for(int i = 0; i < enums.size(); i++)
					_enum.add(enums.get(i).asText());
				
				((io.swagger.models.properties.StringProperty) property).setEnum(_enum);
			}
		}
		else if(type.compareTo("integer") == 0 || type.compareTo("number") == 0)
		{
			property = new io.swagger.models.properties.IntegerProperty();
			
			if(node.has("default"))
				((io.swagger.models.properties.IntegerProperty) property).setDefault(node.get("default").asInt());
			
			if(node.has("maximum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setMaximum(node.get("maximum").asDouble());
			
			if(node.has("minimum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setMinimum(node.get("minimum").asDouble());
			
			if(node.has("exclusiveMaximum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setExclusiveMaximum(node.get("exclusiveMaximum").asBoolean());
			
			if(node.has("exclusiveMinimum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setExclusiveMinimum(node.get("exclusiveMinimum").asBoolean());
			
			if(node.has("enum"))
			{
				ArrayNode enums = (ArrayNode) node.get("enum");
				
				List<Integer> _enum = new ArrayList<Integer>();
				
				for(int i = 0; i < enums.size(); i++)
					_enum.add(enums.get(i).asInt());
				
				((io.swagger.models.properties.IntegerProperty) property).setEnum(_enum);
			}
		}
		else if(type.compareTo("double") == 0)
		{
			property = new io.swagger.models.properties.DoubleProperty();
			
			if(node.has("default"))
				((io.swagger.models.properties.DoubleProperty) property).setDefault(node.get("default").asDouble());
			
			if(node.has("maximum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setMaximum(node.get("maximum").asDouble());
			
			if(node.has("minimum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setMinimum(node.get("minimum").asDouble());
			
			if(node.has("exclusiveMaximum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setExclusiveMaximum(node.get("exclusiveMaximum").asBoolean());
			
			if(node.has("exclusiveMinimum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setExclusiveMinimum(node.get("exclusiveMinimum").asBoolean());
			
			if(node.has("enum"))
			{
				ArrayNode enums = (ArrayNode) node.get("enum");
				
				List<Double> _enum = new ArrayList<Double>();
				
				for(int i = 0; i < enums.size(); i++)
					_enum.add(enums.get(i).asDouble());
				
				((io.swagger.models.properties.DoubleProperty) property).setEnum(_enum);
			}
		}
		else if(type.compareTo("float") == 0)
		{
			property = new io.swagger.models.properties.FloatProperty();
			
			if(node.has("default"))
				((io.swagger.models.properties.FloatProperty) property).setDefault((float) node.get("default").asLong());
			
			if(node.has("maximum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setMaximum(node.get("maximum").asDouble());
			
			if(node.has("minimum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setMinimum(node.get("minimum").asDouble());
			
			if(node.has("exclusiveMaximum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setExclusiveMaximum(node.get("exclusiveMaximum").asBoolean());
			
			if(node.has("exclusiveMinimum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setExclusiveMinimum(node.get("exclusiveMinimum").asBoolean());
			
			if(node.has("enum"))
			{
				ArrayNode enums = (ArrayNode) node.get("enum");
				
				List<Float> _enum = new ArrayList<Float>();
				
				for(int i = 0; i < enums.size(); i++)
					_enum.add((float) enums.get(i).asLong());
				
				((io.swagger.models.properties.FloatProperty) property).setEnum(_enum);
			}
		}
		else if(type.compareTo("long") == 0)
		{
			property = new io.swagger.models.properties.LongProperty();
			
			if(node.has("default"))
				((io.swagger.models.properties.LongProperty) property).setDefault(node.get("default").asLong());
			
			if(node.has("maximum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setMaximum(node.get("maximum").asDouble());
			
			if(node.has("minimum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setMinimum(node.get("minimum").asDouble());
			
			if(node.has("exclusiveMaximum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setExclusiveMaximum(node.get("exclusiveMaximum").asBoolean());
			
			if(node.has("exclusiveMinimum"))
				((io.swagger.models.properties.AbstractNumericProperty) property).setExclusiveMinimum(node.get("exclusiveMinimum").asBoolean());
			
			if(node.has("enum"))
			{
				ArrayNode enums = (ArrayNode) node.get("enum");
				
				List<Long> _enum = new ArrayList<Long>();
				
				for(int i = 0; i < enums.size(); i++)
					_enum.add(enums.get(i).asLong());
				
				((io.swagger.models.properties.LongProperty) property).setEnum(_enum);
			}
		}
		else if(type.compareTo("boolean") == 0)
		{
			property = new io.swagger.models.properties.BooleanProperty();
			
			if(node.has("default"))
				((io.swagger.models.properties.BooleanProperty) property).setDefault(node.get("default").asBoolean());
		}
		else if(type.compareTo("date") == 0)
		{
			property = new io.swagger.models.properties.DateProperty();
			
			if(node.has("default"))
				((io.swagger.models.properties.DateProperty) property).setDefault(node.get("default").asText());
			
			if(node.has("enum"))
			{
				ArrayNode enums = (ArrayNode) node.get("enum");
				
				List<String> _enum = new ArrayList<String>();
				
				for(int i = 0; i < enums.size(); i++)
					_enum.add(enums.get(i).asText());
				
				((io.swagger.models.properties.DateProperty) property).setEnum(_enum);
			}
		}
		else if(type.compareTo("object") == 0)
		{
			property = new io.swagger.models.properties.ObjectProperty();
			
			Map<String, io.swagger.models.properties.Property> properties = new HashMap<String, io.swagger.models.properties.Property>();
			
			ObjectNode fields = (ObjectNode) node.get("properties");
			
			Iterator<String> fieldNames = fields.fieldNames();
			String fieldName;
			
			while(fieldNames.hasNext())
			{
				fieldName = fieldNames.next();
				
				properties.put(fieldName, _buildProperty((ObjectNode) fields.get(fieldName)));
			}
				
			((io.swagger.models.properties.ObjectProperty) property).setProperties(properties);
			
			if(node.has("required"))
			{
				ArrayNode reqs = (ArrayNode) node.get("required");
				
				List<String> required = new ArrayList<String>();
				
				for(int i = 0; i < reqs.size(); i++)
					required.add(reqs.get(i).asText());
				
				((io.swagger.models.properties.ObjectProperty) property).setRequiredProperties(required);
			}				
		}		
		else if(type.compareTo("array") == 0)
		{
			property = new io.swagger.models.properties.ArrayProperty();
			
			ObjectNode items = (ObjectNode) node.get("items");
			
			((io.swagger.models.properties.ArrayProperty) property).setItems(_buildProperty(items));		
			
			if(node.has("maxItems"))
				((io.swagger.models.properties.ArrayProperty) property).setMaxItems(node.get("maxItems").asInt());
			
			if(node.has("minItems"))
				((io.swagger.models.properties.ArrayProperty) property).setMinItems(node.get("minItems").asInt());
		}
		
		if(property != null)
		{
			if(node.has("name"))
				property.setName(node.get("name").asText());
			
			if(node.has("description"))
				property.setDescription(node.get("description").asText());
			
			if(node.has("position"))
				property.setPosition(node.get("position").asInt());
			
			if(node.has("required"))
				property.setRequired(node.get("required").asBoolean());
			
			if(node.has("readOnly"))
				property.setReadOnly(node.get("readOnly").asBoolean());
			
			if(node.has("format"))
				property.setFormat(node.get("format").asText());
			
			if(node.has("example"))
				property.setExample(node.get("example").asText());		
			
			if(node.has("title"))
				property.setTitle(node.get("title").asText());
			
			if(node.has("access"))
				property.setAccess(node.get("access").asText());
		}		
		
		return property;
	}
	
	private io.swagger.models.Model _getModelFromSchema(String jsonschema) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		
		JsonNode node = mapper.readTree(jsonschema);
		
		io.swagger.models.Model schema = null;
		
		if(node.get("type").asText().compareTo("array") == 0)
		{
			schema = new io.swagger.models.ArrayModel();
			
			ObjectNode items = (ObjectNode) node.get("items");			
			
			((io.swagger.models.ArrayModel) schema).setItems(_buildProperty(items));
		}
		else
		{
			schema = new io.swagger.models.ModelImpl();
			
			((io.swagger.models.ModelImpl) schema).setType("object");
			
			Map<String, io.swagger.models.properties.Property> properties = new HashMap<String, io.swagger.models.properties.Property>();
			
			ObjectNode fields = (ObjectNode) node.get("properties");
			
			Iterator<String> fieldNames = fields.fieldNames();
			String fieldName;
			
			while(fieldNames.hasNext())
			{
				fieldName = fieldNames.next();
				
				properties.put(fieldName, _buildProperty((ObjectNode) fields.get(fieldName)));
			}
			
			((io.swagger.models.ModelImpl) schema).setProperties(properties);
			
			if(node.has("required"))
			{
				ArrayNode reqs = (ArrayNode) node.get("required");
				
				List<String> required = new ArrayList<String>();
				
				for(int i = 0; i < reqs.size(); i++)
					required.add(reqs.get(i).asText());
				
				((io.swagger.models.ModelImpl) schema).setRequired(required);
			}		
		}		
		
		return schema;
		
	}
	
	private String describeAsSwagger(BuildingBlock bb) throws Exception
	{		
		Swagger swagger = new Swagger();
		
		swagger.setSwagger("2.0");
		
		Info info = new Info();
		info.setTitle(bb.getTitle());
		info.setDescription(bb.getDescription());
		info.setVersion("1.0");
		
		List<License> licenses = bb.getLegalConditions();
		
		io.swagger.models.License license = null;
		
		if(licenses != null && licenses.size() > 0)
		{	
			license = new io.swagger.models.License();
			license.setName(licenses.get(0).getTitle());
			
			if(licenses.get(0).getUrl().compareTo("") != 0)
				license.setUrl(licenses.get(0).getUrl());
			
			info.setLicense(license);		
		}
		
		swagger.setInfo(info);
		
		List<Tag> tags = new ArrayList<Tag>();
		Tag tag = null;
		
		List<String> ts = bb.getTags();
		
		if(tags != null && ts.size() > 0)
		{
			for(String t : ts)
			{
				tag = new Tag();
				tag.setName(t);
				
				tags.add(tag);
			}
		}
		
		swagger.setTags(tags);
		
		ExternalDocs extDocs = new ExternalDocs();
		extDocs.setUrl(bb.getPage() != null ? bb.getPage() : "");
		
		if(extDocs.getUrl().compareTo("") != 0)
			swagger.setExternalDocs(extDocs);
		
		
		String protocol = "";
		String host = "";
		String basePath = "";
		
		Map<String, Path> paths = new HashMap<String, Path>();
		Path path = null;
		io.swagger.models.Operation operation = null;
		
		List<Operation> ops = bb.getOperations();
		
		List<String> urls = new ArrayList<String>();
		List<String> hosts = new ArrayList<String>();
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		
		if(ops != null && ops.size() > 0)
		{
			
			for(Operation op : ops)
			{
				urls.add(op.getEndpoint());
				
			}
			
			
			String commonPrefix = Utils.longestCommonPrefix(urls);
			
			if(urls.size() > 1)
				commonPrefix = commonPrefix.substring(0, commonPrefix.lastIndexOf("/"));
			
			String[] urlComponents = Utils.getUrlComponents(commonPrefix);
			
			protocol = urlComponents[0];
			host = urlComponents[1];
			basePath = urlComponents[2];
			
			String slash = "";
			
			if(basePath.compareTo("") != 0)
			{
				if(basePath.charAt(basePath.length() - 1) == '/')
				{
					basePath = basePath.substring(0, basePath.length() - 1);
					
					slash = "/";
				}
			}
			
			String _path = "";
			String method = "";
			
			List<String> list = null;
			
			List<io.swagger.models.parameters.Parameter> parameters = null;
			io.swagger.models.parameters.Parameter parameter = null;
			
			Map<String, io.swagger.models.Response> responses = null;
			io.swagger.models.Response response = null;
			
			List<Map<String, List<String>>> security = null;
			Map<String, List<String>> securityElement = null;
			List<String> scopes = null;
			
			List<Parameter> pars = null;
			List<Response> resps = null;
			
			AuthenticationMeasure auth = null;
			System.out.println("stampo auth nullo" + auth);
			List<Permission> permissions = null;
			
			for(Operation op : ops)
			{				
				_path = op.getEndpoint().replace(protocol + "://" + host + basePath , ""); // path of the operation (relative to commonPrefix)
				System.out.println("stampo _path" + _path);
				if(ops.size() == 1)
				{
					_path = basePath;
					basePath = "";
				}
				else
				{
					if(_path.compareTo("") == 0)
					{
						if(basePath.compareTo("") == 0)
						{
							_path = "/";
						}
						else
						{						
							String[] basePathParts = basePath.split("/");
							
							// update basePath
							
							basePath = "";
							
							for(int i = 1; i < basePathParts.length - 1; i++)
							{
								//System.out.println("new-basepath: " + basePath);
								basePath += "/" + basePathParts[i];
							}
							
							_path = "/" + basePathParts[basePathParts.length - 1];
							
							// update inserted paths
							
							if(!paths.isEmpty())
							{
								Iterator<String> keys = paths.keySet().iterator(); // already inserted paths
								
								String key = "";
								Path insPath = null;
								
								while(keys.hasNext())
								{
									key = keys.next();
									insPath = paths.remove(key);
									
									key = "/" + basePathParts[basePathParts.length - 1] + key;
									
									paths.put(key, insPath);
								}
							}						
						}					
					}
				}
				
				if(paths.containsKey(_path))
				{
					path = paths.remove(_path);
				}
				else
				{
					path = new Path();
				}
				
				method = op.getMethod();
				
				operation = new io.swagger.models.Operation();
				
				operation.setDescription(op.getDescription() != null ? op.getDescription() : "");
				
				if(op.getTitle() != null && op.getTitle().compareTo("") != 0)
					operation.setOperationId(op.getTitle());
				
				
				
				list = new ArrayList<String>();
				list.add(op.getConsumes() != null ? op.getConsumes() : "");
				
				operation.setConsumes(list);
				
				list = new ArrayList<String>();
				for (int index= 0; index<op.getProduces().size(); index++){
				
				list.add(op.getProduces().get(index) != null ? op.getProduces().get(index) : "");
				}
				operation.setProduces(list);
				
				pars = op.getParameters();
				
				if(pars != null && pars.size() > 0)
				{
					parameters = new ArrayList<io.swagger.models.parameters.Parameter>();
					
					for(Parameter par : pars)
					{						
						if(par.getIn().equalsIgnoreCase("QUERY"))
						{
							parameter = new QueryParameter();
							
							((QueryParameter) parameter).setType(par.getType());
						}
						else if(par.getIn().equalsIgnoreCase("PATH"))
						{
							parameter = new PathParameter();
							
							((PathParameter) parameter).setType(par.getType());
						}
						else if(par.getIn().equalsIgnoreCase("FORM"))
						{
							parameter = new FormParameter();
							
							((FormParameter) parameter).setType(par.getType());
						}
						else if(par.getIn().equalsIgnoreCase("HEADER"))
						{
							parameter = new HeaderParameter();
							
							((HeaderParameter) parameter).setType(par.getType());
						}
						else if(par.getIn().equalsIgnoreCase("BODY"))
						{
							parameter = new BodyParameter();
							
							String json = par.getSchema();
							
							try 
							{
								((BodyParameter) parameter).setSchema(_getModelFromSchema(json));
									
							} 
							catch (Exception e) 
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						else
						{
							continue;
						}
						
						parameter.setName(par.getName());
						parameter.setIn(par.getIn());
						parameter.setDescription(par.getDescription());
						parameter.setRequired(par.isRequired());
						
						parameters.add(parameter);
						
						
					}
				}
				
				operation.setParameters(parameters);
				
				resps = op.getResponses();
				
				if(resps != null && resps.size() > 0)
				{
					responses = new HashMap<String, io.swagger.models.Response>();
					
					for(Response res : resps)
					{
						response = new io.swagger.models.Response();
						
						response.setDescription(res.getDescription() != null ? res.getDescription() : "");
						
						try 
						{
							if(res.getSchema() != null && res.getSchema().compareTo("") != 0)
								response.setSchema(_buildProperty((ObjectNode) mapper.readTree(res.getSchema())));	
							
						} 
						catch (Exception e) 
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						responses.put(Integer.toString(res.getCode()), response);
					}
				}
				
				operation.setResponses(responses);	
				
				security = new ArrayList<Map<String, List<String>>>();
				securityElement = new HashMap<String, List<String>>();
				scopes = new ArrayList<String>();
				
				auth = (AuthenticationMeasure) op.getSecurityMeasure();
				
				
				
				if(auth != null)
				{
					permissions = auth.getPermissions();
					
					for(Permission perm : permissions)
					{
						scopes.add(perm.getIdentifier());
						
					}
					
					securityElement.put(auth.getTitle(), scopes);
					
					security.add(securityElement);
				}						
				
				
				if(security.size() > 0)
					operation.setSecurity(security);
		            
				
				if(method.equalsIgnoreCase("GET"))
				{
					path.setGet(operation);
				}
				else if(method.equalsIgnoreCase("POST"))
				{
					path.setPost(operation);
				}
				else if(method.equalsIgnoreCase("DELETE"))
				{
					path.setDelete(operation);
				}
				else if(method.equalsIgnoreCase("PATCH"))
				{
					path.setPatch(operation);
				}
				else if(method.equalsIgnoreCase("HEAD"))
				{
					path.setHead(operation);
				}
				else if(method.equalsIgnoreCase("PUT"))
				{
					path.setPut(operation);
				}
				else
				{
					continue;
				}
				
				paths.put(_path, path);
			} // end iterate operations
			
			swagger.setPaths(paths);
		} 
		
		swagger.setHost(host);
		
		if(basePath.compareTo("") != 0)
			swagger.setBasePath(ops.size() != 1 ? basePath : "");
		
		if(protocol.compareTo("") != 0)
		{
			List<Scheme> schemes = new ArrayList<Scheme>();
			schemes.add(Scheme.valueOf(protocol.toUpperCase()));
			
			swagger.setSchemes(schemes);
		}
		
		try 
		{			
			return mapper.writeValueAsString(swagger);
		} 
		catch (JsonProcessingException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return e.getMessage();
		}
	}
	
	private String describeAsWadl(BuildingBlock bb) throws Exception
	{
		return "Not Available";
	}
	
	private String describeAsWsdl(BuildingBlock bb) throws Exception
	{
		return "Not Available";
	}
	
}
