package it.eng.metamodel.mapping;

import it.eng.metamodel.definitions.AuthenticationMeasure;
import it.eng.metamodel.definitions.BuildingBlock;
import it.eng.metamodel.definitions.Entity;
import it.eng.metamodel.definitions.License;
import it.eng.metamodel.definitions.ServiceOffering;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.xmlbeans.XmlException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.java.dev.wadl.x2009.x02.ApplicationDocument;
import net.java.dev.wadl.x2009.x02.ApplicationDocument.Application;
import net.java.dev.wadl.x2009.x02.DocDocument.Doc;
import net.java.dev.wadl.x2009.x02.MethodDocument.Method;
import net.java.dev.wadl.x2009.x02.ParamDocument.Param;
import net.java.dev.wadl.x2009.x02.ParamStyle;
import net.java.dev.wadl.x2009.x02.RepresentationDocument.Representation;
import net.java.dev.wadl.x2009.x02.RequestDocument.Request;
import net.java.dev.wadl.x2009.x02.ResourceDocument.Resource;
import net.java.dev.wadl.x2009.x02.ResourcesDocument.Resources;
import net.java.dev.wadl.x2009.x02.ResponseDocument.Response;

public class WadlMapping implements MetamodelMapping
{
	private static final Logger logger = LoggerFactory.getLogger(WadlMapping.class);
	
	
	public MappingResponse validateDescriptor(String descriptor)
	{
		return _validateDescriptor(descriptor);
	}
	
	
	public MappingResponse validateDescriptor(Object descriptor)
	{
		String d;
		try 
		{
			d = new ObjectMapper().writeValueAsString((ApplicationDocument) descriptor);
		} 
		catch (JsonProcessingException e) 
		{
			logger.error(e.getMessage());
			
			MappingResponse response = new MappingResponse();
			response.addError(e.getMessage());
			
			return response;
		}
		
		return _validateDescriptor(d);
	}
	
	
	public MappingResponse preprocessDescriptor(String descriptor)
	{
		// parse wadl
		ApplicationDocument wadl = null;;
		
		try 
		{
			wadl = ApplicationDocument.Factory.parse(descriptor);
		} 
		catch (XmlException e) 
		{
			logger.error(e.getMessage());
			
			MappingResponse mappingResponse = new MappingResponse();
			mappingResponse.addError(e.getMessage());
			return mappingResponse;
		}
		
		return preprocessDescriptor(wadl);
	}
	
	
	public MappingResponse preprocessDescriptor(Object descriptor)
	{
		MappingResponse response = new MappingResponse();
		
		response.setDescriptor(descriptor);
		response.setDescriptorClass(getDescriptorClass());
		
		return response;
	}
	
	
	public MappingResponse toBuildingBlock(String descriptor)
	{
		// parse wadl
		ApplicationDocument wadl = null;;
		
		try 
		{
			wadl = ApplicationDocument.Factory.parse(descriptor);
		} 
		catch (XmlException e) 
		{
			logger.error(e.getMessage());
			
			MappingResponse mappingResponse = new MappingResponse();
			mappingResponse.addError(e.getMessage());
			return mappingResponse;
		}
		
		return _toBuildingBlock(wadl);
	}
	
	
	public MappingResponse toBuildingBlock(Object descriptor)
	{
		return _toBuildingBlock(descriptor);
	}
	
	
	public Class<?> getDescriptorClass() 
	{
		return ApplicationDocument.class;
	}
	
	private MappingResponse _validateDescriptor(String descriptor)
	{
		MappingResponse response = new MappingResponse();
		
		InputStream main_xsd = null;
		
		String xsdFile = "wadl-schema.xsd";
		
		try 
		{
			main_xsd = WadlMapping.class.getClassLoader().getResourceAsStream(xsdFile);
		} 
		catch (Exception e) 
		{
			logger.error("Error reading " + xsdFile + ":" + e.getMessage());	

			response.addError(e.getMessage());			
			return response;
		}
		
	    try
	    {	    	
	        SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
	        
	        Schema schema = factory.newSchema(new StreamSource(main_xsd));
	        
	        Validator validator = schema.newValidator();
	        validator.setErrorHandler(new XsdErrorHandler());
	        validator.validate(new StreamSource(new ByteArrayInputStream(descriptor.getBytes())));	        
	    }
	    catch(SAXException ex)
	    {
	    	logger.error("Validation error: " + ex.getMessage());
	    	response.addError(ex.getMessage());
	    	return response;
	    } 
	    catch (IOException e) 
	    {
	    	logger.error("Error validating xml");
	    	response.addError(e.getMessage());
	    	return response;
		} 
	    
	    return response;
	}
	
	private MappingResponse _toBuildingBlock(Object descriptor)
	{
		ApplicationDocument wadl = (ApplicationDocument) descriptor;
		
		MappingResponse mappingResponse = new MappingResponse();
		
		BuildingBlock artefact = new BuildingBlock();		
		
		Application app = wadl.getApplication();
		
		// General info
		artefact.setType("REST");		
		artefact.setTitle("");		
		artefact.setDescription(parseDocs(app.getDocList()));
		artefact.setAbstractDescription(parseDocs(app.getDocList()));
		artefact.setPilot("");
		artefact.setCreated("");
		artefact.setPage("");
		
		List<String> tags = new ArrayList<String>();		
		artefact.setTags(tags);
		//
		
		// Business Roles
		artefact.setBusinessRoles(new ArrayList<Entity>());
		// Service Offering
		artefact.setServiceOfferings(new ArrayList<ServiceOffering>());
		
		// Legal Condition
		License license = new License();
		List<License> licenses = new ArrayList<License>();
		licenses.add(license);		
		artefact.setLegalConditions(licenses);
		//	
		
		// descriptor
		artefact.setDescriptorType("WADL");
		
		// Interaction Points
		//List<InteractionPoint> intPoints = new ArrayList<InteractionPoint>();
		//InteractionPoint intPoint = null;
		//
		
		// variables to reuse into loops		
		// Artefact variables
		List<it.eng.metamodel.definitions.Operation> operations = null;
		it.eng.metamodel.definitions.Operation bbOperation = null;	
			
		// wadl variables
		Resources singleResources = null;
		Iterator<Resource> innerResources = null;
		Resource resource = null;
		
		//
		
		// iterate through resources
		
		// retrieve resources		
		Iterator<Resources> resources = app.getResourcesList().iterator();
		
		operations = new ArrayList<it.eng.metamodel.definitions.Operation>();
		
		String baseUrl;
		String path;
		while(resources.hasNext())
		{				
			singleResources = resources.next();
			baseUrl = singleResources.getBase(); 
			
			// iterate through inner resources
			innerResources = singleResources.getResourceList().iterator();
			
			while(innerResources.hasNext())
			{				
				resource = innerResources.next();
				
				operations.addAll(parseResource(resource, baseUrl, new ArrayList<it.eng.metamodel.definitions.Parameter>(), mappingResponse));					
				
				if(operations.size() == 0)
				{
					mappingResponse.addWarning("(" + baseUrl + ") no operation defined");

					continue;
				}
				
			} // end iterate inner resources			
			
		} // end iterate through resources		
					
		artefact.setOperations(operations);
		
		mappingResponse.setBuildingBlock(artefact);
		mappingResponse.setDescriptor(wadl);
		mappingResponse.setDescriptorClass(wadl.getClass());
		
		return mappingResponse;
	}
	
	private String parseDocs(List<Doc> docs){
		
		String response = "";
		
		if(docs != null && docs.size() > 0){
			Doc doc = docs.get(0);
			if(doc != null){
				Node node = doc.getDomNode();
				if(node != null){
					Node child = node.getFirstChild();
					if(child != null){
						response = child.getNodeValue();
					}
				}
			}
		}
		
		return response;
	}
	
	private it.eng.metamodel.definitions.Parameter parseParameter(Param param)
	{
		it.eng.metamodel.definitions.Parameter parameter = new it.eng.metamodel.definitions.Parameter();
		parameter.setName(param.getName());
		parameter.setRequired(param.getRequired());
		parameter.setDescription(parseDocs(param.getDocList()));
		parameter.setType(param.getType().getLocalPart());
		
		if(param.getStyle() == ParamStyle.QUERY)
		{
			parameter.setIn("query");
		}
		else if(param.getStyle() == ParamStyle.TEMPLATE)
		{
			parameter.setIn("path");
		}
		else if(param.getStyle() == ParamStyle.MATRIX)
		{
			parameter.setIn("matrix");
		}
		else if(param.getStyle() == ParamStyle.HEADER)
		{
			parameter.setIn("header");
		}
		else if(param.getStyle() == ParamStyle.PLAIN)
		{
			parameter.setIn("plain");
		}
		else
		{
			return null;
		}
		
		return parameter;
	}
	
	private List<it.eng.metamodel.definitions.Operation> parseResource(Resource resource, String baseUrl, List<it.eng.metamodel.definitions.Parameter> inheritedParameters, MappingResponse mappingResponse)
	{
		List<it.eng.metamodel.definitions.Operation> operations = new ArrayList<it.eng.metamodel.definitions.Operation>();
		
		String path = (resource.getPath().charAt(0) == '/' ? "" : "/") + resource.getPath();
		
		if(baseUrl.endsWith("/"))
			baseUrl = baseUrl.substring(0, baseUrl.length() - 1);
		
		List<Method> methods = resource.getMethodList();

		it.eng.metamodel.definitions.Operation bbOperation = null;
		
		List<it.eng.metamodel.definitions.Parameter> parameters = null;
		it.eng.metamodel.definitions.Parameter parameter = null;
		List<it.eng.metamodel.definitions.Response> responses = null;
		it.eng.metamodel.definitions.Response response = null;	
		Method method = null;
		Request request = null;
		Iterator<Representation> representations = null;
		Iterator<Param> params = null;
		Param param = null;
		Iterator<Response> opResponses = null;
		Response opResponse = null;
		
		// retrieve params defined at resource level
		
		List<it.eng.metamodel.definitions.Parameter> ascendantParameters = new ArrayList<it.eng.metamodel.definitions.Parameter>();
		
		// add inherited parameters
		ascendantParameters.addAll(inheritedParameters);
		
		params = resource.getParamList().iterator();
		
		while(params.hasNext())
		{						
			param = params.next();
			
			parameter = parseParameter(param);
			
			if(parameter != null)
			{
				ascendantParameters.add(parameter);
			}
			else
			{
				mappingResponse.addWarning("(" + path + ") '" + param.getStyle() + "' parameter not supported");
			}
		}
		//
		
		for(int j = 0; j < methods.size(); j++)
		{	
			method = methods.get(j);			
			//System.out.print("\nstampo metodo: " + method);
			bbOperation = new it.eng.metamodel.definitions.Operation();	
			bbOperation.setEndpoint(baseUrl + path);
			bbOperation.setMethod(method.getName());
			bbOperation.setTitle(method.getId() != null && method.getId() != "" ? method.getId() : (method.getName() + " " + path));
			bbOperation.setDescription(parseDocs(method.getDocList()));
			
			String consumes = "";
			List<String> produces = new ArrayList<String>();
			
			boolean supported = true;
			
			request = method.getRequest();	
			//System.out.print("\nStampo la richiesta" + request);
			if(request != null)
			{						
				// populate Operation consumes
				representations = request.getRepresentationList().iterator();
				
				if(representations.hasNext())
					consumes += representations.next().getMediaType();
				
				while(representations.hasNext())
					consumes += "," + representations.next().getMediaType();

				//
				
				// populate Operation parameters
				
				parameters = new ArrayList<it.eng.metamodel.definitions.Parameter>();	
				
				// add parameters from ascendant resources
				parameters.addAll(ascendantParameters);
				System.out.print(method.getRequest().getParamList());
				if(method.getRequest().getParamList() != null)
				{	
					params = method.getRequest().getParamList().iterator();
					
					// iterate through parameters
					while(params.hasNext())
					{						
						param = params.next();
						
						parameter = parseParameter(param);
						
						if(parameter != null)
						{
							parameters.add(parameter);
						}
						else
						{
							mappingResponse.addWarning("(" + method.getName() + " " + path + ") '" + param.getStyle() + "' parameter not supported");
						}
						
					} // end iterate through parameters
				}
								if(parameters.size() == 0)
				{
					mappingResponse.addWarning("(" + method.getName() + " " + path + ") parameters are missing"); 
				}
				else
				{
					for(int p = parameters.size() - 1; p >= 0; p--)
					{
						if(parameters.get(p).getIn().compareToIgnoreCase("header") == 0 && parameters.get(p).getName().compareToIgnoreCase("authorization") == 0)
						{
							parameters.remove(p);
							
							mappingResponse.addWarning("(" + method.getName() + " " + path + ") this method contains an authorization header parameter. you need to set authentication measure at operation level");
							
							AuthenticationMeasure authMeasure = new AuthenticationMeasure();
							authMeasure.setTitle("Authorization");
							
							bbOperation.setSecurityMeasure(authMeasure);
							
							break;
						}
					}					
				}
				
				bbOperation.setParameters(parameters);
			}
			/*else
			{
				mappingResponse.addWarning("(" + method.getName() + " " + path + ") request is missing");
			}*/
			//
			
			// populate Operation responses
			
			responses = new ArrayList<it.eng.metamodel.definitions.Response>();
			
			if(method.getResponseList() != null)
			{
				opResponses = method.getResponseList().iterator();
				
				List status;
				
				while(opResponses.hasNext())
				{
					supported = true;
					
					opResponse = opResponses.next();
					status = opResponse.getStatus();
					
					if(status != null)
					{
						for(int i = 0; i < status.size(); i++)
						{
							response = new it.eng.metamodel.definitions.Response();
							
							try 
							{
								response.setCode(Integer.parseInt(status.get(i).toString()));
							} 
							catch(NumberFormatException e) 
							{
								mappingResponse.addWarning("(" + method.getName() + " " + path + ") '" + status.get(i).toString() + "' response status not supported");
								supported = false;
							}
							
							response.setDescription(parseDocs(opResponse.getDocList()));	
							
							if(supported)
								responses.add(response);
						}
					}
					else
					{
						mappingResponse.addWarning("(" + method.getName() + " " + path + ") response status not specified");
					}
					
					// populate Operation consumes
					representations = opResponse.getRepresentationList().iterator();
					
					if(produces != null)
					{
						if(representations.hasNext())
							produces.add( representations.next().getMediaType());
					}
					
					while(representations.hasNext())
					{
						String mediaType = representations.next().getMediaType();
						
						//String[] tmp = produces.split(",");
						
						for(int index=0; index < produces.size(); index ++)
							if(produces.get(index) != mediaType)
							{
								produces.add( "," + mediaType);
								break;
							}
					}
					//
				}
			}
			else
			{
				mappingResponse.addWarning("(" + method.getName() + " " + path + ") responses are missing"); 
			}
			
			bbOperation.setResponses(responses);
			bbOperation.setProduces(produces);
			
			operations.add(bbOperation);
			
		} // end iterate through methods
		
		
		List<Resource> subResources = resource.getResourceList();
		
		List<it.eng.metamodel.definitions.Parameter> forwardParameters = new ArrayList<it.eng.metamodel.definitions.Parameter>();
		
		for(int i = 0; i < ascendantParameters.size(); i++)
		{
			if(ascendantParameters.get(i).getIn().compareToIgnoreCase("path") == 0 || ascendantParameters.get(i).getIn().compareToIgnoreCase("matrix") == 0)
				forwardParameters.add(ascendantParameters.get(i));
		}
		
		// add operations from sub-resources
		if(subResources.size() > 0)
		{
			for(int i = 0; i < subResources.size(); i++)
			{				
				operations.addAll(parseResource(subResources.get(i), baseUrl + path, forwardParameters, mappingResponse));
			}
		}
		
		return operations;
	}
	
	class XsdErrorHandler implements ErrorHandler {

	    
	    public void warning(SAXParseException exception) throws SAXException {
	        handleMessage("Warning", exception);
	    }

	    
	    public void error(SAXParseException exception) throws SAXException {
	        handleMessage("Error", exception);
	    }

	    
	    public void fatalError(SAXParseException exception) throws SAXException {
	        handleMessage("Fatal", exception);
	    }

	    private String handleMessage(String level, SAXParseException exception) throws SAXException {
	        int lineNumber = exception.getLineNumber();
	        int columnNumber = exception.getColumnNumber();
	        String message = exception.getMessage();
	        throw new SAXException("[" + level + "] line: " + lineNumber + "; column: " + columnNumber + "; cause: " + message);
	    }
	}
}
