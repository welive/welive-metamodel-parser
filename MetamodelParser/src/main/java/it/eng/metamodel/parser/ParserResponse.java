package it.eng.metamodel.parser;

import it.eng.metamodel.mapping.MappingResponse;

public class ParserResponse extends MappingResponse
{
	private boolean valid;
	private boolean preprocessed;
	private boolean parsed;
	
	public ParserResponse() {
		super();

		valid = false;
		preprocessed = false;
		parsed = false;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public boolean isPreprocessed() {
		return preprocessed;
	}

	public void setPreprocessed(boolean preprocessed) {
		this.preprocessed = preprocessed;
	}

	public boolean isParsed() {
		return parsed;
	}

	public void setParsed(boolean parsed) {
		this.parsed = parsed;
	}	
	
	public void setMappingResponse(MappingResponse mappingResponse)
	{
		this.setBuildingBlock(mappingResponse.getBuildingBlock());
		this.setDescriptor(mappingResponse.getDescriptor());
		this.setDescriptorClass(mappingResponse.getDescriptorClass());
		this.setErrors(mappingResponse.getErrors());
		this.setWarnings(mappingResponse.getWarnings());
	}
}
