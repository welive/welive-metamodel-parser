package it.eng.metamodel.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.util.Json;
import it.eng.metamodel.definitions.Dataset;
import it.eng.metamodel.definitions.PublicServiceApplication;
import it.eng.metamodel.mapping.BuildingBlockMapping;
import it.eng.metamodel.mapping.MetamodelMapping;
import it.eng.metamodel.mapping.SwaggerMapping;
import it.eng.metamodel.mapping.WadlMapping;
import it.eng.metamodel.mapping.WsdlMapping;
import it.eng.metamodel.utils.Utils;

public class MetamodelParser 
{
	public static enum SourceType {
		METAMODEL, SWAGGER, WADL, WSDL
	}
	
	public static List<String> splitDescriptor(String descriptor) throws JSONException 
	{
		//System.out.println(descriptor);
		List<String> descriptors = new ArrayList<String>();
		
		
		JSONObject extractDescriptor = new JSONObject(descriptor);
		String descriptorNew = extractDescriptor.toString();
		
		System.out.println(descriptorNew);
		String [] temp = descriptorNew.split("hasOperation");
		String initialDesc= temp[0];
		//System.out.println(initialDesc);
		String currentDescriptor;
		temp = temp[1].split("uses");
		String finalDesc = temp[1];
		//System.out.println(finalDesc);
		List<String> hosts = new ArrayList <String>();
		
		JSONArray opArray = extractDescriptor.optJSONArray("hasOperation");
		JSONObject op = new JSONObject();
		List<String> endpoints = new ArrayList <String>();
		if(opArray != null) {
			int size = opArray.length();
			for(int i=0;i<size;i++) {
				//Prendo l'operation
				op = opArray.getJSONObject(i);
				//System.out.println(op.toString());
				//prendo gli endpoints
				endpoints.add(i, op.getString("endpoint"));
				
				String[] urlComponents = Utils.getUrlComponents(endpoints.get(i));
				
				
				String host = urlComponents[1];
				
				
				hosts.add(i, host);
				
		
			}
			
			String currentHost; 
			System.out.print("\n Stampo gli host" + hosts);
			
			for (int z=0; z<hosts.size(); z++)
			{
				currentHost=hosts.get(z);
				currentDescriptor = initialDesc + "hasOperation\":[ " + opArray.getJSONObject(z).toString();
				//System.out.println(currentDescriptor);
				for (int j=z+1; j<hosts.size(); j++ )
				{
					if(hosts.get(j).equalsIgnoreCase(currentHost))
					{
						currentDescriptor = currentDescriptor + "," + opArray.getJSONObject(j).toString();
						hosts.remove(j);
						opArray.remove(j);
						j = j -1; 
						
					}
				}
				
				System.out.print("\n stampo gli host dopo il for"+ hosts);
				currentDescriptor = currentDescriptor + "],\"uses" + finalDesc;
				//System.out.println(currentDescriptor);
				descriptors.add(currentDescriptor);
			
			}

			//System.out.println(descriptors.get(1));
			
			
		}
		return descriptors;
	}
	
	public static String fixOldDescriptor(String descriptor) throws JSONException {

		JSONObject parsedDescriptor = new JSONObject(descriptor);
		
		JSONArray fixedOperation = new JSONArray();
		JSONArray opArray = parsedDescriptor.optJSONArray("hasOperation");
		if(opArray != null) {
			int size = opArray.length();
			for(int i=0;i<size;i++) {
				//Prendo l'operation
				JSONObject op = opArray.getJSONObject(i);
				//Prendo il produces dell'operation
				JSONArray produces = op.optJSONArray("produces");
				
				//Se non è un array
				if(produces == null) {
					String p = op.getString("produces");
					JSONArray producesArray = new JSONArray();
					
					//Creo un array a partire dalla stringa
					if(p.trim().length() == 0) p = "text";
					producesArray.put(p);
					
					//Aggiorno l'operation
					op.put("produces", producesArray);
				}
				
				//Salvo l'operation modificata
				fixedOperation.put(op);
			}
			
			parsedDescriptor.put("hasOperation",fixedOperation);
		} 
		
		return parsedDescriptor.toString();
	}
	
	public static ParserResponse parse(String descriptor, SourceType type) throws JSONException
	{
		
		MetamodelMapping mapping = null;
		List<MetamodelMapping> mappings;
		
		if(type == SourceType.METAMODEL)
		{
			descriptor = fixOldDescriptor(descriptor);
			
			mapping = new BuildingBlockMapping();
			
			
		}
		else if(type == SourceType.SWAGGER)
		{
			mapping = new SwaggerMapping();
		}
		else if(type == SourceType.WADL)
		{
			mapping = new WadlMapping();
		}
		else if(type == SourceType.WSDL)
		{
			mapping = new WsdlMapping();
		}
		
		return parseRoutine(mapping, descriptor);
	}
	
	public static PublicServiceApplication parsePSA(String descriptor) throws JsonParseException, JsonMappingException, IOException 
	{
		PublicServiceApplication psa = null;
		
		psa = new ObjectMapper().readValue(descriptor, PublicServiceApplication.class);
		
		return psa;
		
	}
	
	public static Dataset parseDataset(String descriptor) throws JsonParseException, JsonMappingException, IOException 
	{
		Dataset dataset = null;
		
		dataset = new ObjectMapper().readValue(descriptor, Dataset.class);
		
		return dataset;
		
	}
	
	public static List<String> convertDescriptor(String descriptor, SourceType inType, SourceType outType) throws JSONException
	{
		List<String> descriptors = new ArrayList<String>();
		List<String> mappings = new ArrayList<String>();
		
		
		descriptors = splitDescriptor(descriptor);
		
		ParserResponse response;
		BuildingBlockMapping mapping = new BuildingBlockMapping();
		for (int i = 0; i<descriptors.size(); i++) {
		
		response = parse(descriptors.get(i), inType);
		
		
		
		
		mappings.add(mapping.describeAs(response.getBuildingBlock(), outType));
		
		
		}
		
		return mappings;
	}
	
	private static ParserResponse parseRoutine(MetamodelMapping mapping, String descriptor)
	{
		ParserResponse response = new ParserResponse();
		
		try
		{		
			// 1. validate
			response.setMappingResponse(mapping.validateDescriptor(descriptor));
			
			if(response.getErrors().size() == 0)
			{ 
				response.setValid(true);
				// 2. pre-process
				response.setMappingResponse(mapping.preprocessDescriptor(descriptor));
				
				if(response.getErrors().size() == 0)
				{
					response.setPreprocessed(true);
					// 3. parse
					response.setMappingResponse(mapping.toBuildingBlock(response.getDescriptor()));
					
					if(response.getErrors().size() == 0)
					{
						response.setParsed(true);
					}
				}
			}
			//
		}
		catch(Exception e)
		{
			response.addError(e.getMessage());
		}
		
		return response;
	}
}
