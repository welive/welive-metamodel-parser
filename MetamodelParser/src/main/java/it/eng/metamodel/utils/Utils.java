package it.eng.metamodel.utils;

import java.util.List;

public class Utils 
{
	private static int longestPrefix(String first, String second) 
	{     
		if(first.length() == 0 || second.length() == 0)
			return 0;
		
	    int maxLen = 0;
	 
	    for(int i = 0; i < first.length(); i++) 
	    {
	        if(i < second.length() && first.charAt(i) == second.charAt(i))
	        	maxLen++;
	        else
	        	break;
	    }

	    return maxLen;
	}
	
	public static String longestCommonPrefix(List<String> strings)
	{
		if(strings == null || strings.size() == 0)
			return "";
		
		if(strings.size() == 1)
			return strings.get(0);
		
		String lcs = strings.get(0);
		
		for(int i = 1; i < strings.size(); i++)
		{
			lcs = lcs.substring(0, longestPrefix(lcs, strings.get(i)));
		}
		
		return lcs;
	}
	
	public static String[] getUrlComponents(String url)
	{
		String[] parts = url.split("/");
		
		String[] components = new String[3];
		
		components[0] = parts[0].replace(":", ""); // protocol
		components[1] = parts[2]; // host
		components[2] = url.substring(parts[0].length() + parts[2].length() + 2); // path
		
		return components;
	}
}
