package it.eng.metamodel.utils;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonUtils 
{
	private static final Logger logger = LoggerFactory.getLogger(JsonUtils.class);
	
	public static JsonNode getNodeByPath(JsonNode node, String path, String subpath)
	{
		String[] steps = path.split("/");
		
		if(steps[0].compareTo("#") != 0)
		{
			logger.error("Invalid path " + path);
			return null;
		}
		
		JsonNode curr = node;
		
		for(int i = 1; i < steps.length; i++)
		{
			if(curr.has(steps[i]))
				curr = curr.get(steps[i]);
			else
			{
				logger.error("Can't find the path " + path);
				return null;
			}
		}
		
		if(subpath != null)
		{
			if(curr.isArray())
			{
				ArrayNode currA = (ArrayNode) curr;
				ArrayNode resA = new ObjectMapper().createArrayNode();
				
				JsonNode res = null;
				
				for(JsonNode n : currA)
				{
					res = getNodeByPath(n, subpath, null);
					
					if(res == null)
					{
						logger.error("Can't find the sub-path " + subpath);
						return null;
					}
					
					resA.add(res);
				}
				
				return resA;
			}
			else if(curr.isObject())
			{
				return getNodeByPath(curr, subpath, null);
			}
			else
			{
				logger.error("Can't find the sub-path " + subpath);
				return null;
			}
		}
		
		return curr;
	}
	
	public static JsonNode setNodeByPath(JsonNode node, String path, JsonNode value)
	{
		String[] steps = path.split("/");
		
		if(steps[0].compareTo("#") != 0)
		{
			logger.error("Invalid path " + path);
			return null;
		}
		
		JsonNode parent = node;
		
		for(int i = 1; i < steps.length - 1; i++)
		{
			if(parent.has(steps[i]))
				parent = parent.get(steps[i]);
			else
			{
				logger.error("Can't find the path " + path);
				return null;
			}
		}
		
		if(parent.has(steps[steps.length - 1]))
		{
			((ObjectNode) parent).set(steps[steps.length - 1], value);
		}
		
		return node;
	}
	
	public static void resolveRefNode(JsonNode parent, JsonNode root) 
	{
        if(parent.has("$ref")) 
        {        
        	String definitionPath = parent.get("$ref").asText(); 
        	
        	JsonNode definition = getNodeByPath(root, definitionPath, null);
        	
        	((ObjectNode) parent).remove("$ref");        	
        	
        	Iterator<String> fields = definition.fieldNames();
        	
        	String field;
        	while(fields.hasNext())
        	{
        		field = fields.next();
        		((ObjectNode) parent).set(field, definition.get(field));
        	}
        }

        // Now, recursively invoke this method on all properties
        for (JsonNode child : parent) {
        	resolveRefNode(child, root);
        }
    }
	
	public static void dereferenceJson(JsonNode node, JsonNode root)
	{
		while(node.findValues("$ref").size() > 0)
			resolveRefNode(node, root);
	}
}
