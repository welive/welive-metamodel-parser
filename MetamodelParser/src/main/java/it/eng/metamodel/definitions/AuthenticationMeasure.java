package it.eng.metamodel.definitions;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenticationMeasure extends SecurityMeasure
{
	private String protocolType;
	@JsonProperty("withIdentityProvider")
	private List<IdentityProvider> identityProviders;
	
	@JsonProperty("requires")
	private List<Permission> permissions;
	
	public AuthenticationMeasure() {
		super();
		
		this.protocolType = "";
		this.identityProviders = new ArrayList<IdentityProvider>();
		this.permissions = new ArrayList<Permission>();	
	}
	
	public AuthenticationMeasure(
			String title, 
			String description,
			String protocolType,
			List<IdentityProvider> identityProviders,
			List<Permission> permissions) {
		super(title, description);

		this.protocolType = protocolType;
		this.identityProviders = identityProviders;
		this.permissions = permissions;		
	}

	public String getProtocolType() {
		return protocolType;
	}

	public void setProtocolType(String protocolType) {
		this.protocolType = protocolType;
	}

	public List<IdentityProvider> getIdentityProviders() {
		return identityProviders;
	}

	public void setIdentityProviders(List<IdentityProvider> identityProviders) {
		this.identityProviders = identityProviders;
	}

	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}
}
