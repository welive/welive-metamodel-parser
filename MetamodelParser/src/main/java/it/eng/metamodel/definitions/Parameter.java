package it.eng.metamodel.definitions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Parameter extends IOVariable
{
	private String name;
	private boolean required;
	@JsonProperty("_in")
	private String in;
	
	public Parameter() {
		super();
		this.name = "";
		this.required = false;
		this.in = "";
	}

	public Parameter(
			
			String description, 
			String schema, 
			String type, 
			String name, 
			boolean required, 
			String in) {
		super(description, schema, type);
		
		this.name = name;
		this.required = required;
		this.in = in;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public String getIn() {
		return in;
	}

	public void setIn(String in) {
		this.in = in;
	}
}
