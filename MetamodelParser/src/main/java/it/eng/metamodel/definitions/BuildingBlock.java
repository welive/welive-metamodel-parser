package it.eng.metamodel.definitions;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BuildingBlock extends Mashuppable 
{
	private String type;
	private String descriptorType;
	private String descriptorUrl;
	
	@JsonProperty("uses")
	private List<Dependency> usedBBs;
	
	/*
	@JsonProperty("hasInteractionPoint")
	private List<InteractionPoint> interactionPoints;
	*/
	
	@JsonProperty("hasOperation")
	private List<Operation> operations;

	public BuildingBlock() {
		super();

		this.type = "";
		this.descriptorType = "";
		this.descriptorUrl = "";
		this.usedBBs = new ArrayList<Dependency>();
		//this.interactionPoints = new ArrayList<InteractionPoint>();
		this.operations = new ArrayList<Operation>();
	}

	public BuildingBlock(String title, String description,
			String abstractDescription, String pilot, String created,
			String page, String language, List<String> tags, List<Entity> businessRoles,
			List<License> legalConditions,
			List<ServiceOffering> serviceOfferings,
			String type, String descriptorType, String descriptorUrl, 
			List<Dependency> usedBBs, 
			//List<InteractionPoint> interactionPoints
			List<Operation> operations) {
		
		super(title, description, abstractDescription, pilot, created, page, language, tags,
				businessRoles, legalConditions, serviceOfferings);

		this.type = type;
		this.descriptorType = descriptorType;
		this.descriptorUrl = descriptorUrl;
		this.usedBBs = usedBBs;
		//this.interactionPoints = interactionPoints;
		this.operations = operations;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Dependency> getUsedBBs() {
		return usedBBs;
	}

	public void setUsedBBs(List<Dependency> usedBBs) {
		this.usedBBs = usedBBs;
	}
	
	/*
	public List<InteractionPoint> getInteractionPoints() {
		return interactionPoints;
	}

	public void setInteractionPoint(List<InteractionPoint> interactionPoints) {
		this.interactionPoints = interactionPoints;
	}
	*/
	
	public String getDescriptorType() {
		return descriptorType;
	}

	public void setDescriptorType(String descriptorType) {
		this.descriptorType = descriptorType;
	}

	public String getDescriptorUrl() {
		return descriptorUrl;
	}

	public void setDescriptorUrl(String descriptorUrl) {
		this.descriptorUrl = descriptorUrl;
	}
	
	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

}
