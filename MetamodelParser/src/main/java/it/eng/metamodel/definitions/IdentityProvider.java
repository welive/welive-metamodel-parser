package it.eng.metamodel.definitions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IdentityProvider 
{
	private String identifier;
	
	public IdentityProvider() {
		super();
		this.identifier = "";
	}
	
	public IdentityProvider(String identifier) {
		super();
		this.identifier = identifier;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}	
}
