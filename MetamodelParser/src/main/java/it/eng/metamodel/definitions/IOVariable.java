package it.eng.metamodel.definitions;

import java.util.List;

public class IOVariable 
{
	private String description;
	private String schema;
	private String type;
	
	
	public IOVariable() {
		super();
		this.description = "";
		this.schema = "";
		this.type = "";
		
	}
	
	public IOVariable(String description, String schema, String type) {
		super();
		this.description = description;
		this.schema = schema;
		this.type = type;
		
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
