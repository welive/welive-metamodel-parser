package it.eng.metamodel.definitions;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Response extends IOVariable 
{
	private int code;

	public Response() {
		super();
		this.code = 200;
	}

	public Response(String description, String schema, String type, int code) {
		super(description, schema, type);
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}
