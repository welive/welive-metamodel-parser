package it.eng.metamodel.definitions;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Artefact 
{
	private String title;
	private String description;
	@JsonProperty("_abstract")
	private String abstractDescription;
	private String pilot;
	private String created;
	private String page;
	private List<String> tags;
	@JsonProperty("language")
	private String lang;
	
	@JsonProperty("hasBusinessRole")
	private List<Entity> businessRoles;
	@JsonProperty("hasLegalCondition")
	private List<License> legalConditions;
	@JsonProperty("hasServiceOffering")
	private List<ServiceOffering> serviceOfferings;
	
	public Artefact() {
		super();
		
		this.title = "";
		this.description = "";
		this.abstractDescription = "";
		this.pilot = "";
		this.created = "";
		this.page = "";
		this.lang = "";
		this.tags = new ArrayList<String>();
		this.businessRoles = new ArrayList<Entity>();
		this.legalConditions = new ArrayList<License>();
		this.serviceOfferings = new ArrayList<ServiceOffering>();
	}
	
	public Artefact(String title, String description,
			String abstractDescription, String pilot, String created,
			String page, String language, List<String> tags, List<Entity> businessRoles,
			List<License> legalConditions,
			List<ServiceOffering> serviceOfferings) {		
		this.title = title;
		this.description = description;
		this.abstractDescription = abstractDescription;
		this.pilot = pilot;
		this.created = created;
		this.page = page;
		this.lang = language;
		this.tags = tags;
		this.businessRoles = businessRoles;
		this.legalConditions = legalConditions;
		this.serviceOfferings = serviceOfferings;
		
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAbstractDescription() {
		return abstractDescription;
	}
	public void setAbstractDescription(String abstractDescription) {
		this.abstractDescription = abstractDescription;
	}
	public String getPilot() {
		return pilot;
	}
	public void setPilot(String pilot) {
		this.pilot = pilot;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public List<String> getTags() {
		return tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public List<Entity> getBusinessRoles() {
		return businessRoles;
	}

	public void setBusinessRoles(List<Entity> businessRoles) {
		this.businessRoles = businessRoles;
	}

	public List<License> getLegalConditions() {
		return legalConditions;
	}

	public void setLegalConditions(List<License> legalConditions) {
		this.legalConditions = legalConditions;
	}

	public List<ServiceOffering> getServiceOfferings() {
		return serviceOfferings;
	}

	public void setServiceOfferings(List<ServiceOffering> serviceOfferings) {
		this.serviceOfferings = serviceOfferings;
	}
	
	public String writeAsJsonString()
	{
		try 
		{
			String json = new ObjectMapper().writeValueAsString(this);
			return json;
		} 
		catch (JsonProcessingException e) 
		{
			e.printStackTrace();
			return null;
		}
	}
}
