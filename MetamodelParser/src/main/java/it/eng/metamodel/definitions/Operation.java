package it.eng.metamodel.definitions;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Operation 
{
	private String title;
	private String description;
	private String endpoint;
	private String method;
	private String consumes; 
	private List<String> produces;
	
	@JsonProperty("hasInput")
	private List<Parameter> parameters;
	@JsonProperty("hasOutput")
	private List<Response> responses;
	@JsonProperty("hasSecurityMeasure")
	private AuthenticationMeasure securityMeasure;
	
	public Operation() {
		super();
		this.title = "";
		this.description = "";
		//this.path = "";
		this.endpoint = "";
		this.method = "";
		this.consumes = "";
		this.produces = new ArrayList<String>();
		this.parameters = new ArrayList<Parameter>();
		this.responses = new ArrayList<Response>();
		this.securityMeasure = new AuthenticationMeasure();
	}
	
	public Operation(String title, String description, String endpoint,
			String method, String consumes, List<String> produces,
			List<Parameter> parameters, List<Response> responses,
			AuthenticationMeasure securityMeasure) {
		super();
		this.title = title;
		this.description = description;
		//this.path = path;
		this.endpoint = endpoint;
		this.method = method;
		this.produces = produces;
		this.consumes = consumes;
		this.parameters = parameters;
		this.responses = responses;
		this.securityMeasure = securityMeasure;
	}

	public Operation(String title, String description, String endpoint,
			String method, String consumes, String produces,
			List<Parameter> parameters, List<Response> responses,
			AuthenticationMeasure securityMeasure) {
		super();
		this.title = title;
		this.description = description;
		//this.path = path;
		this.endpoint = endpoint;
		this.method = method;
		this.produces.add(produces);
		this.consumes = consumes;
		this.parameters = parameters;
		this.responses = responses;
		this.securityMeasure = securityMeasure;
	}
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	/*
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	*/

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getConsumes() {
		return consumes;
	}

	public void setConsumes(String consumes) {
		this.consumes = consumes;
	}

	public void setProducesOld(String producesOld)
	{
		this.produces.add(producesOld);
	
	}
	
	
	public List<String> getProduces() {
		return produces;
	}

	public void setProduces(List<String> produces) {
		this.produces = produces;
	}

	public List<Parameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}

	public List<Response> getResponses() {
		return responses;
	}

	public void setResponses(List<Response> responses) {
		this.responses = responses;
	}

	public SecurityMeasure getSecurityMeasure() {
		return securityMeasure;
	}

	public void setSecurityMeasure(AuthenticationMeasure securityMeasure) {
		this.securityMeasure = securityMeasure;
	}
}
