package it.eng.metamodel.definitions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Dependency {
	
	private Long id;
	private String title;
	
	public Dependency(){
		this.id = null;
		this.title = "";
	}
			
	
	public Dependency(Long id, String title) {
		this.id = id;
		this.title = title;
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	

}
