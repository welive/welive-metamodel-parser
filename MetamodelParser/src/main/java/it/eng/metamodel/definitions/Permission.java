package it.eng.metamodel.definitions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Permission 
{
	private String identifier;
	private String title;
	private String description;
	private String accessType;
	
	public Permission() {
		super();
		this.identifier = "";
		this.title = "";
		this.description = "";
		this.accessType = "";
	}	
	
	public Permission(String identifier, String title, String description, String accessType) {
		super();
		this.identifier = identifier;
		this.title = title;
		this.description = description;
		this.accessType = accessType;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAccessType() {
		return accessType;
	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}	
}
