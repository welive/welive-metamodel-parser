package it.eng.metamodel.definitions;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InteractionPoint 
{

	private String baseurl;
	
	@JsonProperty("hasOperation")
	private List<Operation> operations;

	public InteractionPoint() {
		super();

		this.baseurl = "";
		this.operations = new ArrayList<Operation>();
	}
	
	public InteractionPoint(String baseurl, List<Operation> operations) {
		super();

		this.baseurl = baseurl;
		this.operations = operations;
	}

	public String getBaseurl() {
		return baseurl;
	}

	public void setBaseurl(String baseurl) {
		this.baseurl = baseurl;
	}

	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}
}
