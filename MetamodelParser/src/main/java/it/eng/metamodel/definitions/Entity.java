package it.eng.metamodel.definitions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Entity 
{
	private String title;
	private String page;
	private String mbox;
	private String businessRole;
	
	public Entity() {
		super();		
		this.title = "";
		this.page = "";
		this.mbox = "";
		this.businessRole = "";
	}
	
	public Entity(String title, String page, String mbox, String businessRole) {
		super();
		this.title = title;
		this.page = page;
		this.mbox = mbox;
		this.businessRole = businessRole;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getMbox() {
		return mbox;
	}
	public void setMbox(String mbox) {
		this.mbox = mbox;
	}
	public String getBusinessRole() {
		return businessRole;
	}
	public void setBusinessRole(String businessRole) {
		this.businessRole = businessRole;
	}
	
	
}
