package it.eng.metamodel.definitions;

import java.util.List;

public class Dataset extends Mashuppable
{
	public Dataset() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Dataset(String title, String description,
			String abstractDescription, String pilot, String created,
			String page, String language, List<String> tags, List<Entity> businessRoles,
			List<License> legalConditions,
			List<ServiceOffering> serviceOfferings) {
		super(title, description, abstractDescription, pilot, created, page, language, tags,
				businessRoles, legalConditions, serviceOfferings);
		// TODO Auto-generated constructor stub
	}
}
