In order to export the project:

1. Right-click  on the project

2. Click on Export

3. Click on Java

3. Choose 'Runnable JAR file' and click on 'Next'

4. Select 'Extract required libraries into generated JAR' in the Runnable jar file specification

5. Click on 'Finish'. 