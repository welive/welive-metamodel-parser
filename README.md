# README #

This project is a Maven Java-based project

### What is this repository for? ###

Metamodel Parser

## Requirements  
| Framework                                                                        |Version  |License             |Note 																	|
|--------------------------------------------------------------------------------------------|---------|--------------------|-------------------------------------------------------------------------|
|[Java SE Development Kit](https://www.java.com/it/download/help/index_installing.xml?j=7) |7.0 |[Oracle Binary Code License](http://www.oracle.com/technetwork/java/javase/terms/license/index.html)         |Also v.8|
|[Apache Maven](https://maven.apache.org/download.cgi) |3.2.3| [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)  ||


## Libraries

CDV is based on the following software libraries and frameworks.

|Framework                           |Version       |Licence                                      |
|------------------------------------|--------------|---------------------------------------------|
|[Spring](https://spring.io)| 3.2.3 RELEASE |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Apache Commons Logging](https://commons.apache.org/proper/commons-logging/) | 1.1.1 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[AOP Alliance](http://aopalliance.sourceforge.net) | 1.0 |Public|
|[SLF4J API](https://www.slf4j.org) |1.7.5 |[MIT License](https://www.slf4j.org/license.html) |
|[Logback](https://logback.qos.ch)| 1.0.13 |[EPL v1.0 + LGPL v2.1](https://logback.qos.ch/license.html) |
|[Hibernate](http://hibernate.org)| 5.2.4 | [LGPL v2.1 + ASL v2.0](http://hibernate.org/community/license/) |
|[JBoss Logging](https://github.com/jboss-logging)| 3.2.1 FINAL | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[classmate](https://github.com/FasterXML/java-classmate)| 1.1.0 | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Junit](https://junit.org/junit4/)| 4.11 | [Eclipse Public License v1.0](https://junit.org/junit4/license.html) |
|[Hamcrest](http://hamcrest.org/JavaHamcrest/)| 1.3 | [BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause) |
|[Jackson](https://github.com/FasterXML/jackson)| 2.8.1 | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Swagger Parser](https://github.com/swagger-api/swagger-parser)| 1.0.21 | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Swagger Core](https://github.com/swagger-api/swagger-core)| 1.5.8 | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Joda-Time](http://www.joda.org/joda-time/)| 2.2 | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[SnakeYML](https://mvnrepository.com/artifact/org.yaml/snakeyaml)| 1.12 | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Guava](https://github.com/google/guava)| 18.0 | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Cal10n](http://cal10n.qos.ch)| 18.0 | [MIT License](https://www.slf4j.org/license.html) |
|[Apache XMLBeans](https://xmlbeans.apache.org)| 2.6.0 | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Stax Api](https://docs.oracle.com/javase/tutorial/jaxp/stax/why.html)| 1.0.1 | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Soa-model-core](https://github.com/membrane/soa-model)| 1.5.4 | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Groovy](http://groovy-lang.org)| 2.3.9 | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[HttpComponents](https://hc.apache.org)| 4.2.2 | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Apache Commons Codec](https://commons.apache.org/proper/commons-codec/)| 1.6 | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |




### How do I export the project from Eclipse? ###

In order to export the project:

1. Right-click on the project
2. Click on 'Export'
3. Click on 'Java'
4. Choose 'Runnable JAR file' and click on 'Next'
5. Select 'Extract required libraries into generated JAR' in the Runnable jar file Specification
6. Click on 'Finish'.

### Who do I talk to? ###

ENG team, Chiara Capizzi, Ivan Ligotino

### Copying and License

This code is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)

